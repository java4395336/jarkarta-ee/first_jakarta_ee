package com.example.demo1.servlets;

import com.example.demo1.bean.StudentBean;
import com.example.demo1.entity.Student;
import jakarta.inject.Inject;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "helloServlet", value = "/hello-servlet")
public class HelloServlet extends HttpServlet {
    private String message;

    @Inject
    private StudentBean studentBean;

    public void init() {
        message = "Hello World!";
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");

        List<Student> studentList = studentBean.getAllStudents();

        out.println("<h2>Student List:</h2>");
        out.println("<ul>");

        for (Student student : studentList) {
            out.println("<li>" + student.getName() + "</li>");
        }

        out.println("</ul>");
        out.println("</body></html>");
    }

    public void destroy() {
    }
}