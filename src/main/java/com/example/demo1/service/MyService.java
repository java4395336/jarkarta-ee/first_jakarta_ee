package com.example.demo1.service;

import java.io.Serializable;
import java.util.List;

public interface MyService<T extends Serializable, IdT extends Integer> {
    void create(T entity);
    T findById(IdT id);
    void update(T entity);
    void deleteById(IdT id);
    List<T> findAll();
}
