package com.example.demo1.service.Impl;

import com.example.demo1.entity.Student;
import com.example.demo1.service.StudentService;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;


@ApplicationScoped
@Transactional
public class StudentServiceImpl extends MyServiceImpl<Student, Integer> implements StudentService {
    public StudentServiceImpl() {
        super(Student.class);
    }
}