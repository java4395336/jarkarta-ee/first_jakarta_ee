package com.example.demo1.service.Impl;

import com.example.demo1.service.MyService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;

import java.io.Serializable;
import java.util.List;

public class MyServiceImpl<T extends Serializable, IdT extends Integer> implements MyService<T, IdT> {

    private final Class<T> entityClass;

    @PersistenceContext
    private EntityManager em;

    public MyServiceImpl(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    @Override
    public void create(T entity) {
    }

    @Override
    public T findById(IdT id) {
        return em.find(entityClass, id);
    }

    @Override
    public void update(T entity) {
        // Implementation for updating entity
        // Add your custom logic here
    }

    @Override
    public void deleteById(IdT id) {
        // Implementation for deleting entity by ID
        // Add your custom logic here
    }

    @Override
    public List<T> findAll() {
        return em.createQuery("SELECT e FROM " + entityClass.getSimpleName() + " e", entityClass)
                .getResultList();
    }
}