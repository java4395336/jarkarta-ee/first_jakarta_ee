package com.example.demo1.bean;

import com.example.demo1.entity.Student;
import com.example.demo1.service.StudentService;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;

import java.util.List;

@Stateless
public class StudentBean {

    @Inject
    private StudentService studentService;

    public void createStudent(Student student) {
        studentService.create(student);
    }

    public Student findStudentById(Integer id) {
        return studentService.findById(id);
    }

    public void updateStudent(Student student) {
        studentService.update(student);
    }

    public void deleteStudentById(Integer id) {
        studentService.deleteById(id);
    }

    public List<Student> getAllStudents() {
        return studentService.findAll();
    }
}